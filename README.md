项目编译说明：

FBReaderJ的编译方法是通过ant release的方式，需要安装cygwin,ndk,ant，不过对于新手可能出现很多问题。

我们对FBReader代码进行了小移动，你可以通过如下方式获得代码进使之运行：

1、 git clone https://git.oschina.net/reworkteam/FBReaderJ_cn.git 获得源码。

2、通过Eclipse 的File->New->Other->Android Project From Existing Code导入项目，并且选择四个lib项目
(AmbilWarna,code,library,SuperToasts)也一并导入eclipse.

3、把ndk的路径配置下，使之可以编译JNI。

如果你的sdk是5.0以上，并且安装了google api，那么应该可以运行了。
如果你的sdk是5.0以下，可能需要稍微修改代码才能运行。

到此，可以运行了！


如果有问题，可以到群来咨询！